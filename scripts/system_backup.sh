#/bin/bash
# file system_backup.sh
# date September 2017
# author Adam Zylka
# script backs up whole system excluding a couple of folders
# folders from hdd are mirrored to raspi_server
# restore options can be found in linux file on the desktop

# whole system to hdd
sudo rsync -aAXv --delete --exclude="/dev/*" --exclude="/proc/*" --exclude="/sys/*" --exclude="/tmp/*" --exclude="/run/*" --exclude="/media/*" --exclude="/mnt/*" --exclude="swapfile" --exclude="lost+found" --exclude="/home/adam/.cache" / /media/adam/data/system_backup1/

# hdd folders
SOURCE_DATA=/media/adam/data
# local
# DESTINATION_BACKUP=/media/adam/server_backup
# remote
DESTINATION_BACKUP=adam@nextcloudplus:/media/server_backup1
rsync -aAXv --delete $SOURCE_DATA/applications/   $DESTINATION_BACKUP/applications
rsync -aAXv --delete $SOURCE_DATA/raspi_backup/   $DESTINATION_BACKUP/raspi_backup
rsync -aAXv --delete $SOURCE_DATA/marie_adam/     $DESTINATION_BACKUP/marie_adam
rsync -aAXv --delete $SOURCE_DATA/marie_pictures/ $DESTINATION_BACKUP/marie_pictures
rsync -aAXv --delete $SOURCE_DATA/Pictures/       $DESTINATION_BACKUP/Pictures
rsync -aAXv --delete $SOURCE_DATA/Videos/         $DESTINATION_BACKUP/Videos

# home
SOURCE_HOME_BACKUP=/media/adam/data/system_backup/home/adam/
# local
# DESTINATION_HOME_BACKUP=/media/adam/server_backup/system_backup/home/adam
# remote
DESTINATION_HOME_BACKUP=adam@nextcloudplus:/media/server_backup1/system_backup/home/adam
rsync -aAXv --delete --ignore-missing-args --exclude='/*/.gvfs' $SOURCE_HOME_BACKUP $DESTINATION_HOME_BACKUP


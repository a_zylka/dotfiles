#!/bin/bash
# author Adam Zylka
# date July 2018
# file sync_sailfish.sh
# script uses rsync to backup multiple folders


#############
# CONSTANTS #
#############
SOURCE_HOME=nemo@Sailfish:/home/nemo
SOURCE=nemo@Sailfish:/media/sdcard/9016-4EF8
DESTINATION=/home/adam/nextcloud


#############
# FUNCTIONS #
#############

function_help(){
  echo "syncs following folders from sd card of sailfish:"
  echo " - pictures"
  echo " - videos"
  echo ""
	echo "options:"
	echo "-h or --help -> prints help"
}


function_backup_all(){
  mv -v $SOURCE_HOME/Pictures/Camera/*.jpg  $SOURCE/Pictures/Camera/
  mv -v $SOURCE_HOME/Videos/Camera/*.mp4    $SOURCE/Videos/Camera/
  rsync -aAXv $SOURCE/Pictures/Camera/      $DESTINATION/Photos/Camera
  rsync -aAXv $SOURCE/Videos/Camera/        $DESTINATION/Videos/Camera
}


##############
# RUN SCRIPT #
##############
while :
do
	case "$1" in
		-h | --help) # run help function
			function_help
			exit 0
			;;
		*) # default
			break
			;;
	esac
done

function_backup_all

#!/bin/bash
# author: Adam Zylka
# date: July 2018
# file: setup_dotfiles.sh
# script makes some links to dotfiles and sets up zsh as standard shell

# links
ln -s ~/.dotfiles/conkyrc         ~/.conkyrc
ln -s ~/.dotfiles/gitconfig       ~/.gitconfig
ln -s ~/.dotfiles/tmux/tmux.conf  ~/.tmux.conf
ln -s ~/.dotfiles/vim             ~/.vim
ln -s ~/.dotfiles/vim/vimrc       ~/.vimrc
ln -s ~/.dotfiles/zsh/zshrc       ~/.zshrc

# zsh as standard shell
sudo chsh -s $(which zsh) # for sudo
chsh -s $(which zsh)      # for user

# nvim
mkdir -p ~/.config/nvim
ln -s ~/.dotfiles/neovim/init.vim ~/.config/neovim/
